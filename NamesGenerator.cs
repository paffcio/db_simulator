﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator
{
    public class NamesGenerator
    {
        private readonly NamesContainer _namesContainer;
        private Random _randomNumber;

        public NamesGenerator(NamesContainer namesContainer)
        {
            _namesContainer = namesContainer;
            _randomNumber = new Random();
        }

        public NamesContainer GetNamesContainer()
        {
            return _namesContainer;
        }

        public string GenerateName()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Names[random % _namesContainer.Names.Length];
        }

        public string GenerateSurname()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Surnames[random % _namesContainer.Surnames.Length];
        }

        public string GeneratePosition()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Positions[random % _namesContainer.Positions.Length];
        }

        public string GenerateSpecialization()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Specializations[random % _namesContainer.Specializations.Length];
        }

        public string GenerateDiagnosis()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Diagnosis[random % _namesContainer.Diagnosis.Length];
        }

        public string GeneratePesel()
        {
            int random = _randomNumber.Next();

            return _namesContainer.Pesels[random % _namesContainer.Pesels.Length];
        }
    }

    public class NamesContainer
    {
        public string[] Names { get; set; }
        public string[] Surnames { get; set; }
        public string[] Positions { get; set; }
        public string[] Specializations { get; set; }
        public string[] VisitTypes { get; set; }
        public string[] Diagnosis { get; set; }
        public string [] Pesels { get; set; }
    }
}
