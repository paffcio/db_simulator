﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Symulator.Migrations
{
    public partial class symulator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pacjent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(30)", nullable: true),
                    Surname = table.Column<string>(type: "VARCHAR(50)", nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    PESEL = table.Column<string>(type: "CHAR(12)", nullable: true),
                    RegistrationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pacjent", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RodzajWizyty",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TypeOfVisit = table.Column<string>(type: "VARCHAR(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RodzajWizyty", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Specjalizacja",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SpecializationName = table.Column<string>(type: "VARCHAR(30)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specjalizacja", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Stanowisko",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Position = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stanowisko", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Lekarz",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(50)", nullable: true),
                    Surname = table.Column<string>(type: "VARCHAR(50)", nullable: true),
                    DoctorSpecializationForeignKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lekarz", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Lekarz_Specjalizacja_DoctorSpecializationForeignKey",
                        column: x => x.DoctorSpecializationForeignKey,
                        principalTable: "Specjalizacja",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pracownik",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(30)", nullable: true),
                    Surname = table.Column<string>(type: "VARCHAR(50)", nullable: true),
                    EmployeePositionForeignKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pracownik", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Pracownik_Stanowisko_EmployeePositionForeignKey",
                        column: x => x.EmployeePositionForeignKey,
                        principalTable: "Stanowisko",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Wizyta",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    PatientForeignKey = table.Column<int>(nullable: false),
                    EmployeeForeignKey = table.Column<int>(nullable: false),
                    DoctorForeignKey = table.Column<int>(nullable: false),
                    VisitTypeForeignKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wizyta", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Wizyta_Lekarz_DoctorForeignKey",
                        column: x => x.DoctorForeignKey,
                        principalTable: "Lekarz",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wizyta_Pracownik_EmployeeForeignKey",
                        column: x => x.EmployeeForeignKey,
                        principalTable: "Pracownik",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wizyta_Pacjent_PatientForeignKey",
                        column: x => x.PatientForeignKey,
                        principalTable: "Pacjent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wizyta_RodzajWizyty_VisitTypeForeignKey",
                        column: x => x.VisitTypeForeignKey,
                        principalTable: "RodzajWizyty",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HistoriaWizyty",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Diagnosis = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    VisitForeignKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoriaWizyty", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HistoriaWizyty_Wizyta_VisitForeignKey",
                        column: x => x.VisitForeignKey,
                        principalTable: "Wizyta",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HistoriaWizyty_VisitForeignKey",
                table: "HistoriaWizyty",
                column: "VisitForeignKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lekarz_DoctorSpecializationForeignKey",
                table: "Lekarz",
                column: "DoctorSpecializationForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Pracownik_EmployeePositionForeignKey",
                table: "Pracownik",
                column: "EmployeePositionForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Wizyta_DoctorForeignKey",
                table: "Wizyta",
                column: "DoctorForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Wizyta_EmployeeForeignKey",
                table: "Wizyta",
                column: "EmployeeForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Wizyta_PatientForeignKey",
                table: "Wizyta",
                column: "PatientForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Wizyta_VisitTypeForeignKey",
                table: "Wizyta",
                column: "VisitTypeForeignKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoriaWizyty");

            migrationBuilder.DropTable(
                name: "Wizyta");

            migrationBuilder.DropTable(
                name: "Lekarz");

            migrationBuilder.DropTable(
                name: "Pracownik");

            migrationBuilder.DropTable(
                name: "Pacjent");

            migrationBuilder.DropTable(
                name: "RodzajWizyty");

            migrationBuilder.DropTable(
                name: "Specjalizacja");

            migrationBuilder.DropTable(
                name: "Stanowisko");
        }
    }
}
