﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Employee
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR(30)")]
        public string Name { get; set; }
        [Column(TypeName = "VARCHAR(50)")]
        public string Surname { get; set; }

        public ICollection<Visit.Visit> Visits { get; set; }

        public int EmployeePositionForeignKey { get; set; }
        public virtual EmployeePosition EmployeePosition { get; set; }
    }
}
