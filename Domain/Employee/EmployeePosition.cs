﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Employee
{
    public class EmployeePosition
    {
        [Key]
        public int ID { get; set; }
        public string Position { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
