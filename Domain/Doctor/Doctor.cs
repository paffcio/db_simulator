﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Doctor
{
    public class Doctor
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR(50)")]
        public string Name { get; set; }
        [Column(TypeName = "VARCHAR(50)")]
        public string Surname { get; set; }

        public int DoctorSpecializationForeignKey { get; set; }
        public virtual DoctorSpecialization DoctorSpecialization { get; set; }

        public ICollection<Visit.Visit> Visits { get; set; }
    }
}
