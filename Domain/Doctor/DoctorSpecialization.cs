﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Doctor
{
    public class DoctorSpecialization
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR(30)")]
        public string SpecializationName { get; set; }

        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}
