﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Patient
{
    public class Patient
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "VARCHAR(30)")]
        public string Name { get; set; }
        [Column(TypeName = "VARCHAR(50)")]
        public string Surname { get; set; }
        public DateTime Date { get; set; }
        [Column(TypeName = "CHAR(12)")]
        public string PESEL { get; set; }
        public DateTime RegistrationDate { get; set; }

        public ICollection<Visit.Visit> Visits { get; set; } 
    }
}
