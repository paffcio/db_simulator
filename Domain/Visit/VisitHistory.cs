﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Visit
{
    public class VisitHistory
    {
        [Key]
        public int ID { get; set; }

        [Column(TypeName = "VARCHAR(100)")]
        public string Diagnosis { get; set; }

        public int VisitForeignKey { get; set; }
        public virtual Visit Visit { get;set; }
    }
}
