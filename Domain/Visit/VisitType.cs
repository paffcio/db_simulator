﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Domain.Visit
{
    public class VisitType
    {
        [Key]
        public int ID { get; set; }

        [Column(TypeName = "VARCHAR(50)")]
        public string TypeOfVisit { get; set; }

        public virtual ICollection<Visit> Visit { get; set; }

    }
}
