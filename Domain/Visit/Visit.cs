﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Symulator.Domain.Doctor;
using Symulator.Domain.Employee;

namespace Symulator.Domain.Visit
{
    public class Visit
    {
        [Key]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string Hour
        {
            get
            {
                return Date.ToString("HH:mm");
            }
        }

        public int PatientForeignKey { get; set; }
        public virtual Patient.Patient Patient { get; set; }

        public int EmployeeForeignKey { get; set; }
        public virtual Employee.Employee Employee { get; set; }

        public int DoctorForeignKey { get; set; }
        public virtual Doctor.Doctor Doctor { get; set; }

        public int VisitTypeForeignKey { get; set; }
        public virtual VisitType VisitType { get; set; }
    }
}
