﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Symulator.Context;
using Symulator.Domain.Doctor;
using Symulator.Domain.Employee;
using Symulator.Domain.Patient;
using Symulator.Domain.Visit;

namespace Symulator.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SymulatorContext _context;

        private IRepository<Doctor> _doctorRepository;
        private IRepository<DoctorSpecialization> _doctorSpecializationRepository;
        private IRepository<Employee> _employeeRepository;
        private IRepository<EmployeePosition> _employeePositionRepository;
        private IRepository<Patient> _patientRepository;
        private IRepository<Visit> _visitRepository;
        private IRepository<VisitHistory> _visitHistoryRepository;
        private IRepository<VisitType> _visitTypeRepository;

        public UnitOfWork(SymulatorContext context)
        {
            _context = context;
        }

        public IRepository<Doctor> DoctorRepository
        {
            get
            {
                return _doctorRepository = _doctorRepository ?? new Repository<Doctor>(_context);
            }
        }
        public IRepository<DoctorSpecialization> DoctorSpecilizationRepository
        {
            get
            {
                return _doctorSpecializationRepository = _doctorSpecializationRepository ?? new Repository<DoctorSpecialization>(_context);
            }
        }
        public IRepository<Employee> EmployeeRepository
        {
                get
                {
                    return _employeeRepository = _employeeRepository ?? new Repository<Employee>(_context);
                }
        }
        public IRepository<EmployeePosition> EmployeePositionRepository
        {
            get
            {
                return _employeePositionRepository = _employeePositionRepository ?? new Repository<EmployeePosition>(_context);
            }
        }
        public IRepository<Patient> PatientRepository
        {
            get
            {
                return _patientRepository = _patientRepository ?? new Repository<Patient>(_context);
            }
        }
        public IRepository<Visit> VisitRepository
        {
            get
            {
                return _visitRepository = _visitRepository ?? new Repository<Visit>(_context);
            }
        }
        public IRepository<VisitHistory> VisitHistoryRepository
        {
            get
            {
                return _visitHistoryRepository = _visitHistoryRepository ?? new Repository<VisitHistory>(_context);
            }
        }
        public IRepository<VisitType> VisitTypeRepository
        {
            get
            {
                return _visitTypeRepository = _visitTypeRepository ?? new Repository<VisitType>(_context);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
