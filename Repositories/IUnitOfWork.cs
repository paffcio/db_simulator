﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Symulator.Repositories;
using Symulator.Domain.Doctor;
using Symulator.Domain.Employee;
using Symulator.Domain.Patient;
using Symulator.Domain.Visit;

namespace Symulator.Repositories
{
    public interface IUnitOfWork
    {
        IRepository<Doctor> DoctorRepository { get; }
        IRepository<DoctorSpecialization> DoctorSpecilizationRepository { get; }
        IRepository<Employee> EmployeeRepository { get; }
        IRepository<EmployeePosition> EmployeePositionRepository { get; }
        IRepository<Patient> PatientRepository { get; }
        IRepository<Visit> VisitRepository { get;  }
        IRepository<VisitHistory> VisitHistoryRepository {get; }
        IRepository<VisitType> VisitTypeRepository { get; }
        void Save();
    }
}
