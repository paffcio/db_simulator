﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Symulator.Repositories
{
    public interface IRepository<TEntity>
    {
        IEnumerable<TEntity> GetAllItems();
        int RandomID();
        void CreateItem(TEntity item);
    }
}
