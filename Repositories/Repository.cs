﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Symulator.Context;

namespace Symulator.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly SymulatorContext _context;

        public Repository(SymulatorContext context)
        {
            _context = context;
        }

        public int RandomID()
        {
            var randomNumber = new Random();
            return randomNumber.Next() % _context.Set<TEntity>().Count() + 1;
        }

        public void CreateItem(TEntity item)
        {
            _context.Set<TEntity>().Add(item);
        }

        public IEnumerable<TEntity> GetAllItems()
        {
            return _context.Set<TEntity>().ToList();
        }
    }
}
