﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Symulator.Context;
using Symulator.Domain.Doctor;
using Symulator.Domain.Employee;
using Symulator.Domain.Patient;
using Symulator.Domain.Visit;
using Symulator.Repositories;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;

namespace Symulator.Controllers
{
    [Route("write/[controller]")]
    [ApiController]
    public class WriteController : Controller
    {
        SymulatorContext _context;
        IUnitOfWork _unitOfWork;
        NamesGenerator _namesGenerator;


        public WriteController(SymulatorContext context)
        {
            _context = context;
            _unitOfWork = new UnitOfWork(_context);

            using (StreamReader file = System.IO.File.OpenText(Properties.Resources.NamesFile))
            {
                var container = JsonConvert.DeserializeObject<NamesContainer>(file.ReadToEnd());
                _namesGenerator = new NamesGenerator(container);
            }

        }
        [Route("/addspecializations")]
        public string AddDoctorSpecializations()
        {
            if(_unitOfWork.DoctorSpecilizationRepository.GetAllItems().Count() == 0)
            {
                foreach(var specialization in _namesGenerator.GetNamesContainer().Specializations)
                {
                    _unitOfWork.DoctorSpecilizationRepository.CreateItem(new DoctorSpecialization
                    {
                        SpecializationName = specialization
                    });
                }
            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addpositions")]
        public string AddEmployeesPositions()
        {
            if (_unitOfWork.EmployeePositionRepository.GetAllItems().Count() == 0)
            {
                foreach (var position in _namesGenerator.GetNamesContainer().Positions)
                {
                    _unitOfWork.EmployeePositionRepository.CreateItem(new EmployeePosition
                    {
                        Position = position
                    });
                }
            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addtypevisits")]
        public string AddTypeVisits()
        {
            if (_context.VisitType.Count() == 0)
            {
                foreach (var visitType in _namesGenerator.GetNamesContainer().VisitTypes)
                {
                    _unitOfWork.VisitTypeRepository.CreateItem(new VisitType
                    {
                        TypeOfVisit = visitType
                    });
                }
            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addpatients/{id}")]
        public string AddPatients(int id)
        {
            for(int i = 0; i < id; i++)
            {
                _unitOfWork.PatientRepository.CreateItem(
                    new Patient
                    {
                        Name = _namesGenerator.GenerateName(),
                        Surname = _namesGenerator.GenerateSurname(),
                        Date = DateTime.Now,
                        PESEL = _namesGenerator.GeneratePesel(),
                        RegistrationDate = DateTime.Now,
                    });
            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addemployees/{id}")]
        public string AddEmployees(int id)
        {
            for (int i = 0; i < id; i++)
            {
                var employeePosition = _namesGenerator.GeneratePosition();
                var employeePositionForeignKey = _unitOfWork.EmployeePositionRepository.GetAllItems().Where(p => p.Position == employeePosition).Select(p => p.ID).First();

                _unitOfWork.EmployeeRepository.CreateItem(
                    new Employee
                    {
                        Name = _namesGenerator.GenerateName(),
                        Surname = _namesGenerator.GenerateSurname(),
                        EmployeePositionForeignKey = employeePositionForeignKey,
                    });

            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/adddoctors/{id}")]
        public string AddDoctors(int id)
        {
            for (int i = 0; i < id; i++)
            {
                var doctorSpecialization = _namesGenerator.GenerateSpecialization();
                var doctorSpecializationForeignKey = _unitOfWork.DoctorSpecilizationRepository.GetAllItems().Where(p => p.SpecializationName == doctorSpecialization).Select(p => p.ID).First();

                _unitOfWork.DoctorRepository.CreateItem(
                    new Doctor
                    {
                        Name = _namesGenerator.GenerateName(),
                        Surname = _namesGenerator.GenerateSurname(),
                        DoctorSpecializationForeignKey = doctorSpecializationForeignKey,
                    });

            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addvisits/{id}")]
        public string AddVisits(int id)
        {
            for (int i = 0; i < id; i++)
            {
                var typeVisitForeignKey = _unitOfWork.VisitTypeRepository.RandomID();
                var doctorForeignKey = _unitOfWork.DoctorRepository.RandomID();
                var patientForeignKey = _unitOfWork.PatientRepository.RandomID();
                var employeeForeignKey = _unitOfWork.EmployeeRepository.RandomID();

                _unitOfWork.VisitRepository.CreateItem(
                    new Visit
                    {
                        Date = DateTime.Now,
                        DoctorForeignKey = doctorForeignKey,
                        EmployeeForeignKey = employeeForeignKey,
                        PatientForeignKey = patientForeignKey,
                        VisitTypeForeignKey = typeVisitForeignKey
                    });

            }
            _unitOfWork.Save();

            return "ok";
        }

        [Route("/addhistoryvisits")]
        public string AddHistoryVisits()
        {
            var list = _unitOfWork.VisitRepository.GetAllItems().ToList();
            for(int i = _unitOfWork.VisitHistoryRepository.GetAllItems().Count(); i < list.Count(); i++)
            {
                _unitOfWork.VisitHistoryRepository.CreateItem(
                    new VisitHistory
                    {
                        VisitForeignKey = list[i].ID,
                        Diagnosis = _namesGenerator.GenerateDiagnosis()
                    });
            }
            _unitOfWork.Save();

            return "ok";
        }


        [Route("/executetest/{id}")]
        public string Test(int id)
        {
            AddDoctorSpecializations();

            AddEmployeesPositions();

            AddTypeVisits();

            AddPatients(id);

            AddEmployees(id);

            AddDoctors(id);

            AddVisits(50);

            AddHistoryVisits();

            return "Test has been passed successful.";
        }

    }
}
