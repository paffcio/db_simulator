﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Symulator.Domain.Doctor;
using Symulator.Domain.Visit;
using Symulator.Domain.Patient;
using Symulator.Domain.Employee;

namespace Symulator.Context
{
    public class SymulatorContext : DbContext
    {
        public SymulatorContext(DbContextOptions<SymulatorContext> options)
            : base(options)
        {

        }

        public DbSet<VisitHistory> VisitHistory { get; set; }
        public DbSet<VisitType> VisitType { get; set; }
        public DbSet<Visit> Visit { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<EmployeePosition> EmployeePosition { get; set; }
        public DbSet<Patient> Patient { get; set; }
        public DbSet<Doctor> Doctor { get; set; }
        public DbSet<DoctorSpecialization> DoctorSpecialization { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VisitType>().ToTable("RodzajWizyty");
            modelBuilder.Entity<VisitHistory>().ToTable("HistoriaWizyty");
            modelBuilder.Entity<Visit>().ToTable("Wizyta");
            modelBuilder.Entity<Patient>().ToTable("Pacjent");
            modelBuilder.Entity<Doctor>().ToTable("Lekarz");
            modelBuilder.Entity<DoctorSpecialization>().ToTable("Specjalizacja");
            modelBuilder.Entity<Employee>().ToTable("Pracownik");
            modelBuilder.Entity<EmployeePosition>().ToTable("Stanowisko");

            modelBuilder.Entity<DoctorSpecialization>()
                .HasMany<Doctor>(s => s.Doctors)
                .WithOne(s => s.DoctorSpecialization)
                .HasForeignKey(q => q.DoctorSpecializationForeignKey);

            modelBuilder.Entity<EmployeePosition>()
                .HasMany<Employee>(s => s.Employees)
                .WithOne(s => s.EmployeePosition)
                .HasForeignKey(q => q.EmployeePositionForeignKey);

            modelBuilder.Entity<VisitHistory>()
                .HasOne<Visit>(p => p.Visit)
                .WithOne()
                .HasForeignKey<VisitHistory>(q => q.VisitForeignKey);

            modelBuilder.Entity<VisitType>()
                .HasMany<Visit>(p => p.Visit)
                .WithOne(s => s.VisitType)
                .HasForeignKey(p => p.VisitTypeForeignKey);

            modelBuilder.Entity<Patient>()
                .HasMany<Visit>(s => s.Visits)
                .WithOne(s => s.Patient)
                .HasForeignKey(q => q.PatientForeignKey);

            modelBuilder.Entity<Employee>()
                .HasMany<Visit>(s => s.Visits)
                .WithOne(s => s.Employee)
                .HasForeignKey(q => q.EmployeeForeignKey);

            modelBuilder.Entity<Doctor>()
                .HasMany<Visit>(s => s.Visits)
                .WithOne(s => s.Doctor)
                .HasForeignKey(q => q.DoctorForeignKey);

            /*modelBuilder.Entity<Visit>()
                .HasOne<Patient>(s => s.Patient)
                .WithMany(s => s.Visits)
                .HasForeignKey(p => p.PatientForeignKey);

            modelBuilder.Entity<Visit>()
                .HasOne<Employee>(s => s.Employee)
                .WithMany(s => s.Visits)
                .HasForeignKey(p => p.EmployeeForeignKey);

            modelBuilder.Entity<Visit>()
                .HasOne<Doctor>(s => s.Doctor)
                .WithMany(s => s.Visits)
                .HasForeignKey(p => p.DoctorForeignKey);*/
        }

    }
}
